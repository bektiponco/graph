import React from 'react';
import logo from './logo.svg';
import './App.css';
import Chart from "./Chart";
import {host} from "./utils";


const data = [
  {
    _id: 'Page A', count: 4000, pv: 2400, amt: 2400,
  },
  {
    _id: 'Page B', count: 3000, pv: 1398, amt: 2210,
  },
  {
    _id: 'Page C', count: 2000, pv: 9800, amt: 2290,
  },
  {
    _id: 'Page D', count: 2780, pv: 3908, amt: 2000,
  },
  {
    _id: 'Page E', count: 1890, pv: 4800, amt: 2181,
  },
  {
    _id: 'Page F', count: 2390, pv: 3800, amt: 2500,
  },
  {
    _id: 'Page G', count: 3490, pv: 4300, amt: 2100,
  },
];

class App extends React.Component {
  constructor(props){
    super(props)
    this.state={
      shipData:[]
    }
  }
  componentDidMount() {
    this.getShipData("device_id")
  }

  getShipData=(ket)=>{
    fetch(host + "/chart/ais_data/distribution?query="+ket,
        {
          method: 'GET'
        })
        .then((respone)=>respone.json())
        .then((responJson)=>{
          this.setState({shipData:responJson.result});
        })
        .catch((error)=>console.log(error))
  }
  render() {
    return (
        <div className="App">
           <Chart data={this.state.shipData}/>
        </div>
    );
  }
}

export default App;
